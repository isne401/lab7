#pragma once
#ifndef GRAPH
#define GRAPH

class Node {
public:
	char name = 'x';
	char prev = 0;
	int cost,fscost=0;
	Node *nextdes, *nexts; //next destination , next source
	Node()
	{
		nextdes = nexts = 0;
	}
	Node(char el, Node *n = 0, Node *p = 0)
	{
		name = el; nexts = n; nextdes = p;
	}
};

class graph {
public:
	int nnode = 0;
	graph() { head = tail = 0; }
	int isEmpty() { return head == 0; }
	//~graph();
	void pushDesNode(char from, char el, int cost);
	void pushSourceNode(char el);
	bool search(char el);
	bool checkmulti();
	bool checkpsudo();
	bool weightgraph();
	bool checkcompl();
	bool checkdi();
	void print();
	void dijsktra(int[]);
	Node *head, *tail;
private:
	//Node *head, *tail;
};

#endif //GRAPH