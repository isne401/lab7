// graph.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "graph.h"
using namespace std;
int main()
{
	graph mlist;
	int nnode, val;
	cout << "Please Enter number of nodes : ";
	cin >> nnode;
	mlist.nnode = nnode;
	char aa, bb;
	int** ary = new int*[nnode];
	for (int i = 0; i < nnode; ++i)
		ary[i] = new int[nnode];

	for (int i = 0; i < nnode; i++)
	{
		for (int j = 0; j < nnode; j++)
		{
			cout << "Enter distance ";
			val = 65 + i;
			aa = val;
			cout << aa;

			val = 65 + j;
			bb = val;
			cout << "to " << bb << " ";

			cin >> ary[i][j];
		}
	}
	cout << endl << endl;
	/*for (int i = 0; i < nnode; i++)
	{
	for (int j = 0; j < nnode; j++)
	{
	cout << ary[i][j] << " ";
	}
	cout << endl;
	}*/

	//linklist start here
	for (int i = 0; i < nnode; i++)
	{
		int ascii = 65 + i;
		char name = ascii;
		mlist.pushSourceNode(name);
		for (int j = 0; j < nnode; j++)
		{
			char name2 = ascii + j;
			mlist.pushDesNode(name, name2, ary[i][j]);
		}
	}


	int* access = new int[nnode]; //contain accessible of node 0 = can't access 1=can access
	for (int i = 0; i < nnode; i++)
	{
		access[i] = 0;
	}
	cout << "Dijkstra�s shortest path "<<"\n_______________________________"<<"\nEnter start node :";
	char start;
	cin >> start;
	int y=start,correctinput=0;
	while (correctinput != 1)
	{
		y = start;
		for (int i = 0; i < nnode; i++)
		{
			if (65 + i == y)
			{
				correctinput = 1;
			}
		}
		if (correctinput == 0)
		{
			cout << "invalid input"<<"\nEnter new input :";
			cin >> start;
		}
	}
	access[y - 65] = 1;
	mlist.dijsktra(access);
	int x;
	cin >> x;
	return 0;
}